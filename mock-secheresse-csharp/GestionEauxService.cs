﻿using mock_secheresse_csharp.Dependencies;

namespace mock_secheresse_csharp
{
    public class GestionEauxService
    {

        private IInfoSecheresseService infoSecheresse;
        private IEtatService serviceEtat;
        private IMunicipalServices servicesMunicipaux;

        GestionEauxService(
            IInfoSecheresseService infoSecheresse,
            IEtatService serviceEtat,
            IMunicipalServices servicesMunicipaux)
        {

            this.infoSecheresse = infoSecheresse;
            this.serviceEtat = serviceEtat;
            this.servicesMunicipaux = servicesMunicipaux;
        }
        public void CheckEtatSecheresse()
        {

        }
    }
}
