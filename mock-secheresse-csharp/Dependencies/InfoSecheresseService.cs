﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mock_secheresse_csharp.Dependencies
{
    public interface IInfoSecheresseService
    {
        /**
         * retourne une prévision du nombre de jours sans pluie à venir
         */
        public int PrevisionDureeSecheresse();

        /**
         * retourne l'état des réserves d'eau potable du village (en m3)
         */
        public double ReserveEauMunicipale();
    }
}
