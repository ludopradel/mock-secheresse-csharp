﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mock_secheresse_csharp.Dependencies
{
    public interface IEtatService
    {
        /**
         * Permet d'alerter l'état du manque critique d'eau sur votre commune
         */
        public void SendAlerteEau();
    }
}
