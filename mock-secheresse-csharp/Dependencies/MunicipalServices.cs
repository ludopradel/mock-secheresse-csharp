﻿namespace mock_secheresse_csharp.Dependencies
{
    public interface IMunicipalServices
    {
        /**
         * Envoie un message d'alerte aux habitants
         * pour indiquer les restrictions d'eau en cours
         */
        public void SendRestrictionEauInformation();

        /**
         * Appelle un service de livraison d'eau par camion citernes
         * @throws CiterneVideException lorsque les citernes ne peuvent plus livrer d'eau
         */
        public void CallLivraisonCiterneEau();
    }
}
